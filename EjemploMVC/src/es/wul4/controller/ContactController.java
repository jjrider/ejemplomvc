package es.wul4.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import es.wul4.form.Contact;
import es.wul4.validator.ContactValidator;

@Controller
@SessionAttributes
public class ContactController {

	@Autowired
	@Qualifier(value = "contactValidator")
	private ContactValidator validator;
	
	@RequestMapping(value = "/addContact", method = RequestMethod.POST)
	public ModelAndView addContact(@Valid Contact contact, BindingResult result,
			Map model) {
		Map<String, Object> hmDatos = new HashMap<String, Object>();
		validator.validate(contact, result);
		result.getModel().get("contact");
		BindingResult errors = new BindException(result.getModel().get("contact"), "contact");  
		if (result.hasErrors()) {
			hmDatos.put("contact",  result.getModel().get("contact"));
			return new ModelAndView("contact", hmDatos);
		}
		System.out.println("First Name:" + contact.getFirstname()
				+ "Last Name:" + contact.getLastname());
		return new ModelAndView("contact", "contact", new Contact());

	}

	@RequestMapping("/contacts")
	public ModelAndView showContacts() {
		Map<String, Object> hmDatos = new HashMap<String, Object>();
		hmDatos.put("contact",  new Contact());
		return new ModelAndView("contact",hmDatos);
	}
}
