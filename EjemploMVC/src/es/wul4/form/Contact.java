package es.wul4.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class Contact {
    
	@NotEmpty(message = "This field must not be blank.")
	private String firstname;
	@NotEmpty(message = "This field must not be blank.")
	private String lastname;
	@NotEmpty(message = "This field must not be blank.")
	@Email(message = "Mail bad format.")
	private String email;
	@NotEmpty(message = "This field must not be blank.")
	private String telephone;
    
    
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
 
   
 
}