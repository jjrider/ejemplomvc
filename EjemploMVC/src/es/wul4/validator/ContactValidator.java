package es.wul4.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.wul4.form.Contact;

@Repository(value="contactValidator")
public class ContactValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		
		return Contact.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Pattern p = Pattern.compile("^[6,9]{1}[0-9]{8}$");
		Contact objeto = (Contact) target;
		Matcher m = p.matcher(objeto.getTelephone());
		if(!m.matches()){
			errors.rejectValue("telephone","validation.badformat");
		}

	}

}
